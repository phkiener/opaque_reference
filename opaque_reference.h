/*
 * 2019-01-30 Philipp Kiener: Public Domain.
 *
 * This is free and unencumbered software released into the public domain.
 * 
 * Anyone is free to copy, modify, publish, use, compile, sell, or
 * distribute this software, either in source code form or as a compiled
 * binary, for any purpose, commercial or non-commercial, and by any
 * means.
 * 
 * In jurisdictions that recognize copyright laws, the author or authors
 * of this software dedicate any and all copyright interest in the
 * software to the public domain. We make this dedication for the benefit
 * of the public at large and to the detriment of our heirs and
 * successors. We intend this dedication to be an overt act of
 * relinquishment in perpetuity of all present and future rights to this
 * software under copyright law.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 * 
 * For more information, please refer to <http://unlicense.org>
 */
#pragma once

#include <functional>

/**
 * An opaque reference is a write-only reference, basically an inverse-const.
 * 
 * Similar to an OutputIterator, the underlying value cannot be retrieved, as
 * there is none. Using /operator=/, a value can be assigned. Instead of writing
 * the value directly, a callback function is invoked which handles actually
 * setting the value (or doing whatever).
 * 
 * Returning opaque references is an alternative to writing a /set_foo/-method
 * which allows the syntax
 * 
 * \code
 * foo.bar = quuz;
 * \endcode
 * 
 * to set /bar/ to /quuz/ without providing a way to store the value of /bar/.
 * 
 * This can be used to imitate something along the lines of the following C#
 * snippet:
 * 
 * \code
 * class Example {
 *     public int Value { private get; set; }
 * }
 * \endcode
 * 
 * Using opaque references, this can be implemented as such:
 * 
 * \code
 * class example {
 *     private:
 *         int _value;
 * 
 *     public:
 *         const opaque_reference<int> value =
 *             opaque_reference([this](auto value){ _value = value; });
 * }
 * \endcode
 */
template <typename T>
class opaque_reference {
    public:
        /**
         * Construct an opaque reference, using the supplied function as setter.
         * 
         * @param[in] setter the callback for writing the assigned value.
         */
        opaque_reference(std::function<void(const T&)> setter);

        /**
         * Write a value to the reference.
         * 
         * @param[in] value the value to write
         * @return          the written value
         */
        const opaque_reference<T>& operator= (const T& value) const;

    private:
        /**
         * The callback to call when a value is assigned.
         */
        const std::function<void(const T&)> _setter;
};

template<typename T>
opaque_reference<T>::opaque_reference(std::function<void(const T&)> setter) :
    _setter(setter) {

}

template<typename T>
const opaque_reference<T>& opaque_reference<T>::operator= (const T& value) const {
    _setter(value);
    return *this;
}

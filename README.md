# opaque_reference

A small header-only utility library for opaque references.

## What's an opaque reference?

Basically, an opaque reference is the inverse of a const reference: you can write, but you can't read. The only allowed operation on an opaque reference is the assignment operator `operator=`.

## Usage

This is a header-only library consisting of a single header. Just drop `opaque_reference` anywhere in your project and include it. It's public domain, so you need not credit me in any way.

## Why would I use these?

If you dabbled a bit into C# territory, you might have used _properties_. _Properties_ allow neat things like public getters and private setters, but also private getters and public setters.

```C#
using System;
					
public class Example {
	public int Value { private get; set; }
	
	public Example(int start) {
		Value = start;
	}
	
	public override string ToString() {
		return Value.ToString();	
	}
}

public class Usage {
	public static void Main(string[] args) {
		Example foo = new Example(10);
		
		Console.WriteLine("Value is: " + foo.ToString());
		foo.Value = 100;
		Console.WriteLine("Value is: " + foo.ToString());
		
		// var stored = foo.Value; nope, not allowed!
	}
}
```

This will print

> Value is: 10  
> Value is: 100

Uncomment that last statement, however, and you'll get

> Compilation error (line 23, col 16): The property or indexer 'example.Value' cannot be used in this context because the get accessor is inaccessible

Neato! With an `opaque_reference`, you can imitate this behaviour in C++.

```C++
#include <iostream>
#include <string>

#include "opaque_reference.h"

class example {
    private:
        int _value;

    public:
        const opaque_reference<int> value;

        example(int start) :
            _value(start),
            value([this](auto x){_value = x;})
            {}

        std::string to_string() const {
            return std::to_string(_value);
        }
};

int main() {
    example foo(10);

    std::cout << "Value is: " << foo.to_string() << "\n";
    foo.value = 100;
    std::cout << "Value is: " << foo.to_string() << "\n";

    // int stored = foo.value; nope, not allowed!
}
```

This will print exactly the same as the C# snippet does. Uncomment the last line, and you'll get an error about `opaque_reference<int>` not being convertible to `int`.
When using `auto` though, it will work. But that `auto` will be `opaque_reference<int>`, which will yield you the same problem again.

## That's gotta be a performance hit, ain't it?

It is a performance hit indeed. 10 million writes to an `int` took the following times:

| Method                  | Time (ns) |
|-------------------------|----------:|
| `opaque_reference<int>` | 274938770 | 
| `int&`                  |  25224533 |

Takes around 10 times longer when using the opaque reference. It's the price you pay for nice syntax!

## So basically it's a glorified callback with some sugar?

I... well...

Yes.
